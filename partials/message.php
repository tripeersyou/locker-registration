<?php if (isset($_SESSION['message'])): ?>
<article class="message is-info">
  <div class="message-body">
    <?= $_SESSION['message'] ?>
  </div>
</article>
<?php unset($_SESSION['message']) ?>
<?php endif ?>

<?php if (isset($_SESSION['warning'])): ?>
<article class="message is-warning">
  <div class="message-body">
    <?= $_SESSION['warning'] ?>
  </div>
</article>
<?php unset($_SESSION['warning']) ?>
<?php endif ?>
