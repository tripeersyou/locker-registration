<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'student' && isset($_SESSION['edited'])): ?>
    <?php header('Location: ./index.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'&& !isset($_SESSION['registered'])): ?>
<?php include('../partials/header.php'); ?>
<?php include('../partials/message.php'); ?>
    <h1 class="title">Student Information</h1>
    <form action="./php/edit_info.php" method="post">
        <div class="field">
            <label for="ID Number" class="label">ID Number</label>
            <input type="text" class="input" value="<?= $_SESSION['id_number']; ?>" disabled>        
        </div>
        <div class="field">
            <label for="Full Name" class="label">Full Name</label>
            <input type="text" class="input" name="name" required>
        </div>
        <div class="field">
            <label for="Course" class="label">Course</label>
            <input type="text" class="input" name="course" required>
        </div>
        <div class="field">
            <label for="Year" class="label">Year</label>
            <div class="control">
                <div class="select">
                    <select name="year" id="year" required>
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="field">
            <label for="email" class="label">Email</label>
            <input type="text" class="input" name="email" required>
        </div>
        <div class="field">
            <label for="email" class="label">Cellphone</label>
            <input type="text" maxlength="11" class="input" name="cellphone" required>
        </div>
        <button class="button">Submit</button>
    </form>
<?php include('../partials/footer.php'); ?>
<?php elseif(isset($_SESSION['registered'])):?>
    <?php header('Location: ./evaluation.php') ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'): ?>
    <?php header('Location: ../index.php'); ?>
<?php else: ?>
    <?php header('Location: ../login.php'); ?>
<?php endif ?>