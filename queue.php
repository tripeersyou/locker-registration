<?php session_start(); ?>
<?php include_once('./php/db_connect.php'); 
    $query = "SELECT * FROM queue;";
    $result = $db->query($query);
?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin' && !isset($_SESSION['registration'])): ?>
<?php include('./partials/header.php'); ?>
<?php include('./partials/message.php'); ?>
<h1 class="is title">Student Login Queue</h1>
<a href="./students.php" class="button">Back</a>
<div class="columns">
    <div class="column">
        <table class="table">
            <thead>
                <tr>
                    <th>ID Number</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php while($student = $result->fetch_assoc()): ?>
                <tr>
                    <td><?= $student['student_id']; ?></td>
                    <td>
                        <form action="./php/dequeue.php" method="post">
                            <input type="hidden" name="id_number" value="<?= $student['student_id']; ?>">
                            <button type="submit" class="button">Dequeue</button>
                        </form>
                    </td>
                </tr>
                <?php endwhile?>
            </tbody>
        </table>
    </div>
    <div class="column">
        <form action="./php/queue_student.php" method="post">
            <div class="field">
                <label for="id_number" class="label">ID Number</label>
                <input type="text" name="id_number" class="input" id="id_number" maxlength="6">
            </div>
        </form>
    </div>
</div>
<?php include('./partials/footer.php'); ?>
<?php elseif(isset($_SESSION['registration'])): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php else: ?>
    <?php header('Location: ./login.php'); ?>
<?php endif ?>