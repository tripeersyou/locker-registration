<?php 
    session_start();
    include_once('./db_connect.php');
    if($_POST) {
        $old_username = $_SESSION['admin_username'];
        $new_username = $db->real_escape_string(htmlspecialchars($_POST['username']));
        $new_password = $db->real_escape_string(htmlspecialchars($_POST['password']));
        $new_password_confirmation = $db->real_escape_string(htmlspecialchars($_POST['confirm_password']));
        $old_password = $db->real_escape_string(htmlspecialchars($_POST['old_password']));
        $salt = hash('md5', $old_password);
        $old_password = hash('sha512', $salt.$old_password);

        $password_valid_query = "SELECT * FROM admins WHERE password = '$old_password';";
        $result = $db->query($password_valid_query);

        if($result->num_rows == 1) {
            if($old_username != $new_username){
                $username_valid_query = "SELECT * FROM admins WHERE username = '$new_username';";
                $result = $db->query($username_valid_query);
                if($result->num_rows == 0){
                    if($new_password == $new_password_confirmation) {
                        $salt = hash('md5', $new_password);
                        $new_password = hash('sha512', $salt.$new_password);
                        $update_query = "UPDATE admins SET username = '$new_username', password ='$new_password' WHERE username = '$old_username';";
                        $db->query($update_query);
                        $_SESSION['message'] = "Account successfully updated";
                        header('Location: ../index.php');
                    } else {
                        # New passwords don't match
                        $_SESSION['warning'] = "Passwords don't match";
                        header('Location: ../settings.php');
                    }
                } else {
                    # Username has been taken
                    $_SESSION['warning'] = "Username has been taken";
                    header('Location: ../settings.php');
                }
            } else {
                # Same username
                if($new_password == $new_password_confirmation) {
                    $salt = hash('md5', $new_password);
                    $new_password = hash('sha512', $salt.$new_password);
                    $update_query = "UPDATE admins SET password = '$new_password' WHERE username = '$old_username';";
                    $db->query($update_query);
                    $_SESSION['message'] = "Account successfully updated";
                    header('Location: ../index.php');
                } else {
                    # New passwords don't match
                    $_SESSION['warning'] = "Passwords don't match";
                    header('Location: ../settings.php');
                }
            }
        } else {
            # Invalid old password
            $_SESSION['warning'] = "Invalid password";
            header('Location: ../settings.php');
        }
    }