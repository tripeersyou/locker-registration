<?php
    session_start();
    include_once('../../php/db_connect.php');
    if($_POST){
        $idnumber = htmlspecialchars($db->real_escape_string($_POST['idnumber']));
        $query = "SELECT * FROM queue WHERE student_id = $idnumber";
        $result = $db->query($query);
        if($result->num_rows > 0) {
            $_SESSION['status'] = 'student';
            $_SESSION['id_number'] = $idnumber;
            header("Location: ../edit.php");
        } else {
            header("Location: ../login.php");
        }
    } else {
        header("Location: ../login.php");
    }
?>