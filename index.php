<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin' && !isset($_SESSION['registration'])): ?>
<?php include('./partials/header.php'); ?>
<div class="container">
    <h1 class="title is-1">Administration Panel</h1>
    <a href="./php/destroy_session.php" class="button">Log Out</a>
    <?php include('./partials/message.php'); ?>
    <br>
    <div class="columns">
        <div class="column">
            <div class="card">
                <div class="card-content">
                    <p class="title is-4"><a href="./register/login.php">Start Registration</a></p>
                    <div class="meta">Start the locker registration process.</div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="card">
                <div class="card-content">
                    <p class="title is-4"><a href="./report.php">Evaluation Report</a></p>
                    <div class="meta">View the evaluation of the students regarding the locker registration.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="columns">
        <div class="column">
        <div class="card">
            <div class="card-content">
                <p class="title is-4"><a href="./map.php?id=1">Locker Map</a></p>
                <div class="meta">View all the current lockers. Add more lockers or edit a locker.</div>
            </div>
        </div>
        </div>
        <div class="column">
	        <div class="card">
            <div class="card-content">
                <p class="title is-4"><a href="./students.php">Students</a></p>
                <div class="meta">View all the students that signed up for locker registration and put them in the log-in queue.</div>
            </div>
            </div>
        </div>
        <div class="column">
            <div class="card">
            <div class="card-content">
                <p class="title is-4"><a href="./settings.php">Settings</a></p>
                <div class="meta">Change the settings of your account and create other admin accounts.</div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php include('./partials/footer.php'); ?>
<?php elseif(isset($_SESSION['registration'])): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php else: ?>
    <?php header('Location: ./login.php'); ?>
<?php endif ?>