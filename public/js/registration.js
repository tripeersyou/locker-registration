$(document).ready(()=>{
    $('.locker-button').click(function(){
        let lockerId = $(this).data('value');
        $('.modal-card-body').text(`You have chosen locker ${lockerId}.`);
        $('.modal').addClass('is-active');
        $('.locker-confirm').click(()=>{
            $.post("../register/php/register.php",{id: lockerId}, function(response) {
                if (response === "Valid") {
                    $('.locker-confirm').attr('disabled', true);
                    $('.continue-button').css('display', 'block');
                    $('.modal-close').attr('disabled', true);
                    $('.button-close').attr('disabled', true);
                    $('.modal-card-body').text(`Congratulations, you have registered for ${lockerId}.`);
                } else {
                    $('.locker-confirm').css('display', 'none');
                    $(`#${lockerId}`).attr('disabled', true);
                    $('.modal-card-body').text(`Sorry locker ${lockerId} is already taken.`);
                }
            });
        });
    });
    $('.modal-close').click(function() {
        $('.locker-confirm').attr('disabled', false);
        $('.continue-button').css('display', 'none');
        $('.modal').removeClass('is-active');
    });
    $('.button-close').click(function() {
        $('.locker-confirm').attr('disabled', false);
        $('.continue-button').css('display', 'none');
        $('.modal').removeClass('is-active');
    });
});