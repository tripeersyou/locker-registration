<?php session_start(); ?>
<?php if(!isset($_SESSION['status'])): ?>
<?php include_once('./partials/header.php'); ?>
    <div class="container">
    <?php include_once('./partials/message.php'); ?>    
        <h1 class="title">Admin Login</h1>
        <form action="./php/start_session.php" method="post">
            <div class="field">
                <label for="Username" class="label">Username</label>
                <input type="text" class="input" name="username">
            </div>
            <div class="field">
                <label for="password" class="label">Password</label>
                <input type="password" class="input" name="password">
            </div>
            <button type="submit" class="button">Login</button>
        </form>
    </div>
<?php include_once('./partials/footer.php'); ?>
<?php else: ?>
    <?php header('Location: ./index.php'); ?>
<?php endif ?>