-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2018 at 04:51 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `locker_registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`) VALUES
(1, 'dsws', '14a72c8a732935c3b9f66b9586abf9805f2d15e162edbd5b23617b1e21e376d5b07bf852f48e7d6bdb812ec05f656ef6074b866c9c62a72118a6550f38bc2308');

-- --------------------------------------------------------

--
-- Table structure for table `cabinets`
--

CREATE TABLE `cabinets` (
  `id` int(11) NOT NULL,
  `room` varchar(10) NOT NULL,
  `place_id` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `position` char(1) NOT NULL,
  `row_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cabinets`
--

INSERT INTO `cabinets` (`id`, `room`, `place_id`, `floor`, `position`, `row_number`) VALUES
(1, 'B102', 2, 1, 'L', 8),
(2, 'B102', 2, 1, 'R', 8),
(3, 'B103', 2, 1, 'L', 8),
(4, 'B103', 2, 1, 'R', 8),
(5, 'B104', 2, 1, 'L', 8),
(6, 'B104', 2, 1, 'R', 8),
(7, 'B105', 2, 1, 'L', 8),
(8, 'B105', 2, 1, 'R', 8),
(9, 'B106', 2, 1, 'L', 8),
(10, 'B106', 2, 1, 'R', 8),
(11, 'B205', 2, 2, 'L', 8),
(12, 'B205', 2, 2, 'R', 8),
(13, 'B206', 2, 2, 'L', 8),
(14, 'B206', 2, 2, 'R', 8),
(15, 'B207', 2, 2, 'L', 8),
(16, 'B207', 2, 2, 'R', 8),
(17, 'B208', 2, 2, 'L', 8),
(18, 'B208', 2, 2, 'R', 8),
(19, 'B209', 2, 2, 'L', 8),
(20, 'B209', 2, 2, 'R', 8),
(21, 'K201', 3, 2, 'L', 8),
(22, 'K201', 3, 2, 'R', 8),
(23, 'K202', 3, 2, 'L', 8),
(24, 'K202', 3, 2, 'R', 8),
(25, 'K203', 3, 2, 'L', 8),
(26, 'K203', 3, 2, 'R', 8),
(27, 'K204', 3, 2, 'L', 8),
(28, 'K204', 3, 2, 'R', 8),
(29, 'CTC114', 1, 1, 'C', 8),
(30, 'VENDING', 1, 1, 'C', 8);

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(11) NOT NULL,
  `dsws_knowledge` varchar(50) DEFAULT NULL,
  `registration_knowledge` varchar(50) DEFAULT NULL,
  `competence` int(11) DEFAULT NULL,
  `venue` int(11) DEFAULT NULL,
  `logistics` int(11) DEFAULT NULL,
  `system_use` int(11) DEFAULT NULL,
  `completion_time` int(11) DEFAULT NULL,
  `suggestion` text,
  `comments` text,
  `overall` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lockers`
--

CREATE TABLE `lockers` (
  `id` varchar(10) NOT NULL,
  `cabinet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lockers`
--

INSERT INTO `lockers` (`id`, `cabinet_id`) VALUES
('B001', 1),
('B002', 1),
('B003', 1),
('B004', 1),
('B005', 1),
('B006', 1),
('B007', 1),
('B008', 1),
('B009', 1),
('B010', 1),
('B011', 1),
('B012', 1),
('B013', 1),
('B014', 1),
('B015', 1),
('B016', 1),
('B017', 1),
('B018', 1),
('B019', 1),
('B020', 1),
('B021', 1),
('B022', 1),
('B023', 1),
('B024', 1),
('B025', 2),
('B026', 2),
('B027', 2),
('B028', 2),
('B029', 2),
('B030', 2),
('B031', 2),
('B032', 2),
('B033', 2),
('B034', 2),
('B035', 2),
('B036', 2),
('B037', 2),
('B038', 2),
('B039', 2),
('B040', 2),
('B041', 2),
('B042', 2),
('B043', 2),
('B044', 2),
('B045', 2),
('B046', 2),
('B047', 2),
('B048', 2),
('B049', 3),
('B050', 3),
('B051', 3),
('B052', 3),
('B053', 3),
('B054', 3),
('B055', 3),
('B056', 3),
('B057', 3),
('B058', 3),
('B059', 3),
('B060', 3),
('B061', 3),
('B062', 3),
('B063', 3),
('B064', 3),
('B065', 3),
('B066', 3),
('B067', 3),
('B068', 3),
('B069', 3),
('B070', 3),
('B071', 3),
('B072', 3),
('B073', 4),
('B074', 4),
('B075', 4),
('B076', 4),
('B077', 4),
('B078', 4),
('B079', 4),
('B080', 4),
('B081', 4),
('B082', 4),
('B083', 4),
('B084', 4),
('B085', 4),
('B086', 4),
('B087', 4),
('B088', 4),
('B089', 4),
('B090', 4),
('B091', 4),
('B092', 4),
('B093', 4),
('B094', 4),
('B095', 4),
('B096', 4),
('B097', 5),
('B098', 5),
('B099', 5),
('B100', 5),
('B101', 5),
('B102', 5),
('B103', 5),
('B104', 5),
('B105', 5),
('B106', 5),
('B107', 5),
('B108', 5),
('B109', 5),
('B110', 5),
('B111', 5),
('B112', 5),
('B113', 5),
('B114', 5),
('B115', 5),
('B116', 5),
('B117', 5),
('B118', 5),
('B119', 5),
('B120', 5),
('B121', 6),
('B122', 6),
('B123', 6),
('B124', 6),
('B125', 6),
('B126', 6),
('B127', 6),
('B128', 6),
('B129', 6),
('B130', 6),
('B131', 6),
('B132', 6),
('B133', 6),
('B134', 6),
('B135', 6),
('B136', 6),
('B137', 6),
('B138', 6),
('B139', 6),
('B140', 6),
('B141', 6),
('B142', 6),
('B143', 6),
('B144', 6),
('B145', 7),
('B146', 7),
('B147', 7),
('B148', 7),
('B149', 7),
('B150', 7),
('B151', 7),
('B152', 7),
('B153', 7),
('B154', 7),
('B155', 7),
('B156', 7),
('B157', 7),
('B158', 7),
('B159', 7),
('B160', 7),
('B161', 7),
('B162', 7),
('B163', 7),
('B164', 7),
('B165', 7),
('B166', 7),
('B167', 7),
('B168', 7),
('B169', 8),
('B170', 8),
('B171', 8),
('B172', 8),
('B173', 8),
('B174', 8),
('B175', 8),
('B176', 8),
('B177', 8),
('B178', 8),
('B179', 8),
('B180', 8),
('B181', 8),
('B182', 8),
('B183', 8),
('B184', 8),
('B185', 8),
('B186', 8),
('B187', 8),
('B188', 8),
('B189', 8),
('B190', 8),
('B191', 8),
('B192', 8),
('B193', 9),
('B194', 9),
('B195', 9),
('B196', 9),
('B197', 9),
('B198', 9),
('B199', 9),
('B200', 9),
('B201', 9),
('B202', 9),
('B203', 9),
('B204', 9),
('B205', 9),
('B206', 9),
('B207', 9),
('B208', 9),
('B209', 9),
('B210', 9),
('B211', 9),
('B212', 9),
('B213', 9),
('B214', 9),
('B215', 9),
('B216', 9),
('B217', 10),
('B218', 10),
('B219', 10),
('B220', 10),
('B221', 10),
('B222', 10),
('B223', 10),
('B224', 10),
('B225', 10),
('B226', 10),
('B227', 10),
('B228', 10),
('B229', 10),
('B230', 10),
('B231', 10),
('B232', 10),
('B233', 10),
('B234', 10),
('B235', 10),
('B236', 10),
('B237', 10),
('B238', 10),
('B239', 10),
('B240', 10),
('B241', 11),
('B242', 11),
('B243', 11),
('B244', 11),
('B245', 11),
('B246', 11),
('B247', 11),
('B248', 11),
('B249', 11),
('B250', 11),
('B251', 11),
('B252', 11),
('B253', 11),
('B254', 11),
('B255', 11),
('B256', 11),
('B257', 11),
('B258', 11),
('B259', 11),
('B260', 11),
('B261', 11),
('B262', 11),
('B263', 11),
('B264', 11),
('B265', 12),
('B266', 12),
('B267', 12),
('B268', 12),
('B269', 12),
('B270', 12),
('B271', 12),
('B272', 12),
('B273', 12),
('B274', 12),
('B275', 12),
('B276', 12),
('B277', 12),
('B278', 12),
('B279', 12),
('B280', 12),
('B281', 12),
('B282', 12),
('B283', 12),
('B284', 12),
('B285', 12),
('B286', 12),
('B287', 12),
('B288', 12),
('B289', 13),
('B290', 13),
('B291', 13),
('B292', 13),
('B293', 13),
('B294', 13),
('B295', 13),
('B296', 13),
('B297', 13),
('B298', 13),
('B299', 13),
('B300', 13),
('B301', 13),
('B302', 13),
('B303', 13),
('B304', 13),
('B305', 13),
('B306', 13),
('B307', 13),
('B308', 13),
('B309', 13),
('B310', 13),
('B311', 13),
('B312', 13),
('B313', 14),
('B314', 14),
('B315', 14),
('B316', 14),
('B317', 14),
('B318', 14),
('B319', 14),
('B320', 14),
('B321', 14),
('B322', 14),
('B323', 14),
('B324', 14),
('B325', 14),
('B326', 14),
('B327', 14),
('B328', 14),
('B329', 14),
('B330', 14),
('B331', 14),
('B332', 14),
('B333', 14),
('B334', 14),
('B335', 14),
('B336', 14),
('B337', 15),
('B338', 15),
('B339', 15),
('B340', 15),
('B341', 15),
('B342', 15),
('B343', 15),
('B344', 15),
('B345', 15),
('B346', 15),
('B347', 15),
('B348', 15),
('B349', 15),
('B350', 15),
('B351', 15),
('B352', 15),
('B353', 15),
('B354', 15),
('B355', 15),
('B356', 15),
('B357', 15),
('B358', 15),
('B359', 15),
('B360', 15),
('B361', 16),
('B362', 16),
('B363', 16),
('B364', 16),
('B365', 16),
('B366', 16),
('B367', 16),
('B368', 16),
('B369', 16),
('B370', 16),
('B371', 16),
('B372', 16),
('B373', 16),
('B374', 16),
('B375', 17),
('B376', 17),
('B377', 17),
('B378', 17),
('B379', 17),
('B380', 17),
('B381', 17),
('B382', 17),
('B383', 17),
('B384', 17),
('B385', 17),
('B386', 17),
('B387', 17),
('B388', 17),
('B389', 17),
('B390', 17),
('B391', 17),
('B392', 17),
('B393', 17),
('B394', 17),
('B395', 17),
('B396', 17),
('B397', 17),
('B398', 17),
('B399', 18),
('B400', 18),
('B401', 18),
('B402', 18),
('B403', 18),
('B404', 18),
('B405', 18),
('B406', 18),
('B407', 18),
('B408', 18),
('B409', 18),
('B410', 18),
('B411', 18),
('B412', 18),
('B413', 18),
('B414', 18),
('B415', 18),
('B416', 18),
('B417', 18),
('B418', 18),
('B419', 18),
('B420', 18),
('B421', 18),
('B422', 19),
('B423', 19),
('B424', 19),
('B425', 19),
('B426', 19),
('B427', 19),
('B428', 19),
('B429', 19),
('B430', 19),
('B431', 19),
('B432', 19),
('B433', 19),
('B434', 19),
('B435', 19),
('B436', 19),
('B437', 19),
('B438', 19),
('B439', 19),
('B440', 19),
('B441', 19),
('B442', 19),
('B443', 19),
('B444', 19),
('B445', 19),
('B446', 19),
('B447', 20),
('B448', 20),
('B449', 20),
('B450', 20),
('B451', 20),
('B452', 20),
('B453', 20),
('B454', 20),
('B455', 20),
('B456', 20),
('B457', 20),
('B458', 20),
('B459', 20),
('B460', 20),
('B461', 20),
('B462', 20),
('B463', 20),
('B464', 20),
('B465', 20),
('B466', 20),
('B467', 20),
('B468', 20),
('B469', 20),
('B470', 20),
('B471', 20),
('B472', 20),
('B473', 20),
('B474', 20),
('B475', 20),
('B476', 20),
('B477', 20),
('B478', 20),
('B479', 20),
('B480', 20),
('K001', 21),
('K002', 21),
('K003', 21),
('K004', 21),
('K005', 21),
('K006', 21),
('K007', 21),
('K008', 21),
('K009', 21),
('K010', 21),
('K011', 21),
('K012', 21),
('K013', 21),
('K014', 21),
('K015', 21),
('K016', 21),
('K017', 21),
('K018', 21),
('K019', 21),
('K020', 21),
('K021', 21),
('K022', 21),
('K023', 21),
('K024', 21),
('K025', 22),
('K026', 22),
('K027', 22),
('K028', 22),
('K029', 22),
('K030', 22),
('K031', 22),
('K032', 22),
('K033', 22),
('K034', 22),
('K035', 22),
('K036', 22),
('K037', 22),
('K038', 22),
('K039', 22),
('K040', 22),
('K041', 22),
('K042', 22),
('K043', 22),
('K044', 22),
('K045', 22),
('K046', 22),
('K047', 22),
('K048', 22),
('K049', 23),
('K050', 23),
('K051', 23),
('K052', 23),
('K053', 23),
('K054', 23),
('K055', 23),
('K056', 23),
('K057', 23),
('K058', 23),
('K059', 23),
('K060', 23),
('K061', 23),
('K062', 23),
('K063', 23),
('K064', 23),
('K065', 23),
('K066', 23),
('K067', 23),
('K068', 23),
('K069', 23),
('K070', 23),
('K071', 23),
('K072', 23),
('K073', 24),
('K074', 24),
('K075', 24),
('K076', 24),
('K077', 24),
('K078', 24),
('K079', 24),
('K080', 24),
('K081', 24),
('K082', 24),
('K083', 24),
('K084', 24),
('K085', 24),
('K086', 24),
('K087', 24),
('K088', 24),
('K089', 24),
('K090', 24),
('K091', 24),
('K092', 24),
('K093', 24),
('K094', 24),
('K095', 24),
('K096', 24),
('K097', 25),
('K098', 25),
('K099', 25),
('K100', 25),
('K101', 25),
('K102', 25),
('K103', 25),
('K104', 25),
('K105', 25),
('K106', 25),
('K107', 25),
('K108', 25),
('K109', 25),
('K110', 25),
('K111', 25),
('K112', 25),
('K113', 25),
('K114', 25),
('K115', 25),
('K116', 25),
('K117', 25),
('K118', 25),
('K119', 25),
('K120', 25),
('K121', 26),
('K122', 26),
('K123', 26),
('K124', 26),
('K125', 26),
('K126', 26),
('K127', 26),
('K128', 26),
('K129', 26),
('K130', 26),
('K131', 26),
('K132', 26),
('K133', 26),
('K134', 26),
('K135', 26),
('K136', 26),
('K137', 26),
('K138', 26),
('K139', 26),
('K140', 26),
('K141', 26),
('K142', 26),
('K143', 26),
('K144', 26),
('K145', 27),
('K146', 27),
('K147', 27),
('K148', 27),
('K149', 27),
('K150', 27),
('K151', 27),
('K152', 27),
('K153', 27),
('K154', 27),
('K155', 27),
('K156', 27),
('K157', 27),
('K158', 27),
('K159', 27),
('K160', 27),
('K161', 27),
('K162', 27),
('K163', 27),
('K164', 27),
('K165', 27),
('K166', 27),
('K167', 27),
('K168', 27),
('K169', 28),
('K170', 28),
('K171', 28),
('K172', 28),
('K173', 28),
('K174', 28),
('K175', 28),
('K176', 28),
('K177', 28),
('K178', 28),
('K179', 28),
('K180', 28),
('K181', 28),
('K182', 28),
('K183', 28),
('K184', 28),
('K185', 28),
('K186', 28),
('K187', 28),
('K188', 28),
('K189', 28),
('K190', 28),
('K191', 28),
('K192', 28),
('C001', 29),
('C002', 29),
('C003', 29),
('C004', 29),
('C005', 29),
('C006', 29),
('C007', 29),
('C008', 29),
('C009', 29),
('C010', 29),
('C011', 29),
('C012', 29),
('C013', 29),
('C014', 29),
('C015', 29),
('C016', 29),
('C017', 29),
('C018', 29),
('C019', 29),
('C020', 29),
('C021', 29),
('C022', 29),
('C023', 29),
('C024', 29),
('C025', 30),
('C026', 30),
('C027', 30),
('C028', 30),
('C029', 30),
('C030', 30),
('C031', 30),
('C032', 30),
('C033', 30),
('C034', 30),
('C035', 30),
('C036', 30),
('C037', 30),
('C038', 30),
('C039', 30),
('C040', 30),
('C041', 30),
('C042', 30),
('C043', 30),
('C044', 30),
('C045', 30),
('C046', 30),
('C047', 30),
('C048', 30);

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `status`) VALUES
(1, 'PLDT-CTC', 1),
(2, 'Berchman', 1),
(3, 'Kostka', 1),
(4, 'SEC-A', 1),
(5, 'SEC-C', 1);

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `course` varchar(50) DEFAULT NULL,
  `others` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cellphone` varchar(11) DEFAULT NULL,
  `locker_id` varchar(10) DEFAULT NULL,
  `registration_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `cabinets`
--
ALTER TABLE `cabinets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place_id` (`place_id`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `lockers`
--
ALTER TABLE `lockers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cabinet_id` (`cabinet_id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locker_id` (`locker_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cabinets`
--
ALTER TABLE `cabinets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cabinets`
--
ALTER TABLE `cabinets`
  ADD CONSTRAINT `cabinets_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`);

--
-- Constraints for table `lockers`
--
ALTER TABLE `lockers`
  ADD CONSTRAINT `lockers_ibfk_1` FOREIGN KEY (`cabinet_id`) REFERENCES `cabinets` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`locker_id`) REFERENCES `lockers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
