<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin' && !isset($_SESSION['registration'])): ?>
<?php 
    include_once('./php/db_connect.php'); 
    $query = "SELECT *  FROM places;";
    $result = $db->query($query);
?>
<?php include('./partials/header.php'); ?>
<?php include('./partials/message.php'); ?>
<h1 class="title is-1">Settings</h1>
<a href="./index.php" class="button">Back</a>
<hr>
<div class="columns">
    <div class="column">
        <h3 class="title is-3">Account Settings</h3>
        <form action="./php/account_update.php" method="post">
            <div class="field">
                <label for="username" class="label">Username</label>
                <input type="text" class="input" name="username" value="<?= $_SESSION['admin_username']?>">
            </div>
            <div class="field">
                <label for="password" class="label">New Password</label>
                <input type="password" class="input" name="password">
            </div>
            <div class="field">
                <label for="confirm_password" class="label">Confirm New Password</label>
                <input type="password" class="input" name="confirm_password">
            </div>
            <div class="field">
                <label for="old_password" class="label">Old Password</label>
                <input type="password" class="input" name="old_password" required>
            </div>
            <button type="submit" class="button">Submit</button>
        </form>
    </div>
    <div class="column">
        <h3 class="title is-3">Building Settings</h3>
        <form action="./php/building_update.php" method="post">
            <div class="columns is-multiline">        
                <?php while($place = $result->fetch_assoc()): ?>
                    <div class="column is-one-third">
                        <?php if($place['status']): ?>
                            <label class="checkbox">
                                <input type="checkbox" name="place_status[]" value="<?= $place['id'] ?>" checked>
                                <?= $place['name'] ?>
                            </label>                  
                        <?php else: ?>
                            <label class="checkbox">
                                <input type="checkbox" name="place_status[]" value="<?= $place['id'] ?>">
                                <?= $place['name'] ?>
                            </label>  
                        <?php endif ?>
                    </div>
                <?php endwhile ?>
            </div>
        <button type="submit" class="button">Submit</button>
        </form>
    </div>
</div>
<?php include('./partials/footer.php'); ?>
<?php $db->close(); ?>
<?php elseif(isset($_SESSION['registration'])): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php else: ?>
    <?php header('Location: ./login.php'); ?>
<?php endif ?>