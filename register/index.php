<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'student' && isset($_SESSION['edited']) && !isset($_SESSION['registered'])): ?>
<?php include('../partials/header.php'); ?>
<?php include('../partials/message.php'); ?>
<?php include_once('../php/db_connect.php'); ?>
<script src="../public/js/registration.js"></script>
<?php 
    $query = "SELECT * from places WHERE status = 1";
    $result_building = $db->query($query);
    if (!isset($_GET['id'])){
        $_GET['id'] = -1;
    }
?>
<h1 class="title">Register for a Locker</h1>
<div class="tabs is-medium">
  <ul>
    <?php while($place = $result_building->fetch_assoc()): ?>
        <?php if($_GET['id'] == $place['id']): ?>
            <li class="is-active"><a href="?id=<?= $place['id']?>"><?= $place['name']?></a></li>
        <?php else: ?>
            <li><a href="?id=<?= $place['id']?>"><?= $place['name']?></a></li>
        <?php endif; ?>
    <?php endwhile; ?>
  </ul>
</div>
<?php
    $place = htmlspecialchars($db->real_escape_string($_GET['id']));
    $query = "SELECT lockers.id, places.name, cabinets.room, cabinets.row_number, cabinets.position, cabinets.floor FROM lockers JOIN cabinets ON (cabinets.id = lockers.cabinet_id) JOIN places ON (places.id = cabinets.place_id) WHERE cabinets.place_id = $place";
    $result = $db->query($query);
    $lockers = $result->fetch_all(MYSQLI_NUM);
?>
    <?php if($lockers[0][1] == 'Berchman' || $lockers[0][1] == 'Kostka'): ?>
     <?php for ($i = 0; $i < count($lockers) ; $i++): ?>
        <?php 
            $locker_id = $lockers[$i][0];
            $query = "SELECT COUNT(*) as count FROM students WHERE locker_id = '$locker_id'";
            $result = $db->query($query);
            $count = $result->fetch_all(MYSQLI_NUM);
        ?>
        <?php if($i % 24 == 0): ?>
        <div style="text-align:center">
            <h2 class="title is-4" style="margin:0px;">In front of <?= $lockers[$i][2]?></h1>
            <small class="subtitle">Floor #<?= $lockers[$i][5]?> | <?= $lockers[$i][4] ?> side</small>
            <hr>
        </div>
        <?php endif ?>
        <?php if($i % 8 == 0): ?>
            <div class="columns is-multiline">
        <?php endif ?>
            <div class="column">
                <?php if($count[0][0] > 0): ?>
                    <button class="button is-primary locker-button" data-value="<?= $lockers[$i][0]?>" id="<?= $lockers[$i][0]?>" disabled><?= $lockers[$i][0]?></button>
                <?php else: ?>
                    <button class="button is-primary locker-button" data-value="<?= $lockers[$i][0]?>" id="<?= $lockers[$i][0]?>" ><?= $lockers[$i][0]?></button>
                <?php endif ?> 
            </div>
        <?php if($i % 8 == 7): ?>
            </div>
        <?php endif ?>
        <?php if($i % 24 == 0): ?>
            <br>
        <?php endif ?>
     <?php endfor ?>
    <?php elseif ($lockers[0][1] == 'PLDT-CTC'):?>
    <?php for ($i = 0; $i < count($lockers) ; $i++): ?>
        <?php 
            $locker_id = $lockers[$i][0];
            $query = "SELECT COUNT(*) as count FROM students WHERE locker_id = '$locker_id'";
            $result = $db->query($query);
            $count = $result->fetch_all(MYSQLI_NUM);
        ?>
        <?php if($i % 24 == 0): ?>
        <div style="text-align:center;">
            <h2 class="title is-4" style="margin:0px;">Near <?= $lockers[$i][2]?></h1>
            <small class="subtitle">Floor #<?= $lockers[$i][5]?></small>
            <hr>
        </div>
        <?php endif ?>
        <?php if($i % 8 == 0): ?>
            <div class="columns is-multiline">
        <?php endif ?>
            <div class="column">
                <?php if($count[0][0] > 0): ?>
                    <button class="button is-primary locker-button" data-value="<?= $lockers[$i][0]?>" id="<?= $lockers[$i][0]?>" disabled><?= $lockers[$i][0]?></button>
                <?php else: ?>
                    <button class="button is-primary locker-button" data-value="<?= $lockers[$i][0]?>" id="<?= $lockers[$i][0]?>"><?= $lockers[$i][0]?></button>
                <?php endif ?>
            </div>
        <?php if($i % 8 == 7): ?>
            </div>
        <?php endif ?>
        <?php if($i % 24 == 0): ?>
            <br>
        <?php endif ?>
     <?php endfor ?>
    <?php elseif ($lockers[0][1] == 'SEC-A'):?>
    <?php elseif ($lockers[0][1] == 'SEC-C'):?>
    <?php endif ?>
<div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Confirmation</p>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
    </section>
    <footer class="modal-card-foot">
      <button class="button is-success locker-confirm">Register</button>
      <button class="button button-close">Cancel</button>
      <a class="button is-primary continue-button" style="display:none;" href="./evaluation.php">Continue</a>
    </footer>
  </div>
  <button class="modal-close is-large" aria-label="close"></button>
</div>
<?php include('../partials/footer.php'); ?>
<?php $db->close(); ?>
<?php elseif(isset($_SESSION['registered'])):?>
    <?php header('Location: ./evaluation.php') ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./edit.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'): ?>
    <?php header('Location: ../index.php'); ?>
<?php else: ?>
    <?php header('Location: ../login.php'); ?>
<?php endif ?>