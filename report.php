<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin' && !isset($_SESSION['registration'])): ?>
<?php include('./partials/header.php'); ?>
<?php include('./partials/message.php'); ?>
<?php 
    include_once('./php/db_connect.php');
    $query = "SELECT students.id, students.name, evaluations.dsws_knowledge, evaluations.registration_knowledge, evaluations.competence, evaluations.venue, evaluations.logistics, evaluations.system_use, evaluations.completion_time, evaluations.suggestion, evaluations.comments, evaluations.overall FROM evaluations JOIN students ON (evaluations.student_id = students.id);";
    $result = $db->query($query);
?>
    <h1 class="title">Reports</h1>
    <a href="./index.php" class="button">Back</a>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th>ID Number</th>
                <th>Name</th>
                <th>Knowledge Source</th>
                <th>Registration Knowledge</th>
                <th>Competence of DSWS</th>
                <th>Venue</th>
                <th>Logistics</th>
                <th>System Use</th>
                <th>Completion Time</th>
                <th>Suggestion</th>
                <th>Comments</th>
                <th>Overall</th>
            </tr>
        </thead>
        <tbody>
            <?php while($evaluation = $result->fetch_assoc()): ?>
                <tr>
                    <td><?= $evaluation['id']; ?></td>
                    <td><?= $evaluation['name']; ?></td>
                    <td><?= $evaluation['dsws_knowledge']; ?></td>
                    <td><?= $evaluation['registration_knowledge']; ?></td>
                    <td><?= $evaluation['competence']; ?></td>
                    <td><?= $evaluation['venue']; ?></td>
                    <td><?= $evaluation['logistics']; ?></td>
                    <td><?= $evaluation['system_use']; ?></td>
                    <td><?= $evaluation['completion_time']; ?></td>
                    <td><?= $evaluation['suggestion']; ?></td>
                    <td><?= $evaluation['comments']; ?></td>
                    <td><?= $evaluation['overall']; ?></td>
                </tr>
            <?php endwhile ?>
        </tbody>
    </table>
<?php 
    $db->close();
?>
<?php include('./partials/footer.php'); ?>
<?php elseif(isset($_SESSION['registration'])): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php else: ?>
    <?php header('Location: ./login.php'); ?>
<?php endif ?>