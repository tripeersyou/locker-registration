<?php session_start(); ?>
<?php if(isset($_SESSION['registered'])): ?>
<?php include('../partials/header.php'); ?>
<?php include('../partials/message.php'); ?>
<h1 class="title">Evaluations</h1>
<form method="POST" action="./php/evaluate.php">
    <label class="label">How did you learn about DSWS? Check all that apply.</label>    
    <div class="columns is-multiline">
        <label class="checkbox column">
            <input type="checkbox" name="dsws_knowledge[]" value="seminar">
            Orientation Seminar 
        </label>
        <label class="checkbox column">
            <input type="checkbox" name="dsws_knowledge[]" value="posters">
            Posters, Teasers, etc.
        </label>
        <label class="checkbox column">
            <input type="checkbox" name="dsws_knowledge[]" value="friends">
            Friends/Word of Mouth
        </label>
    </div>
    <input type="checkbox" name="dsws_knowledge[]" value="others">
    Others (please specify below)
    <textarea class="textarea" name="dsws_knowledge[]" cols="30" rows="10"></textarea>
    <br>
    <label class="label">How did you learn about Locker Registration? Check all that apply.</label>
    <div class="columns is-multiline">
        <label class="checkbox column">
            <input type="checkbox" name="registration_knowledge[]" value="seminar">
            Orientation Seminar 
        </label>
        <label class="checkbox column">
            <input type="checkbox" name="registration_knowledge[]" value="posters">
            Posters, Teasers, etc.
        </label>
        <label class="checkbox column">
            <input type="checkbox" name="registration_knowledge[]" value="friends">
            Friends/Word of Mouth
        </label>
    </div>
    <input type="checkbox" name="registration_knowledge[]" value="others">
    Others (please specify below)
    <textarea class="textarea" name="registration_knowledge[]" cols="30" rows="10"></textarea>
    <br>
    <label class="label">Rate the following aspects of the Locker Registration Service (0 being the lowest and 5 being the highest)</label>
    <div class="columns is-multiline">
        <div class="field column">
            <label class="label">Competence of DSWS Facilitators</label>
            <div class="select" style="width: 100%;">
                <select name="competence" style="width: 100%;" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
        <div class="field column">
            <label class="label">Venue of Registration</label>
            <div class="select" style="width: 100%;">
                <select name="venue" style="width: 100%;" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
    </div>
    <div class="columns is-multiline">
        <div class="field column">
            <label class="label">Efficiency of Logistics</label>
            <div class="select" style="width: 100%;">
                <select name="logistics" style="width: 100%;" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
        <div class="field column">
            <label class="label">Ease of Use of the System</label>
            <div class="select" style="width: 100%;">
                <select name="system_use" style="width: 100%;" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
        <div class="field column">
            <label class="label">Response Rate / Completion Time</label>
            <div class="select" style="width: 100%;">
                <select name="completion_time" style="width: 100%;" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
    </div>
    <div class="field">
        <label class="label">Suggestions on how to improve the registration process</label>
        <textarea name="suggestion" class="textarea" cols="30" rows="10" required></textarea>
    </div>
    <div class="field">
        <label class="label">Comments</label>
        <textarea name="comments" class="textarea" cols="30" rows="10" required></textarea>
    </div>
    <div class="field">
        <label class="label">Overall Experience</label>        
        <div class="select" style="width: 100%;">
            <select name="overall" style="width: 100%;" required>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
    </div>
    <button type="submit" class="button is-primary">Submit</button>
</form>
<?php include('../partials/header.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'): ?>
    <?php header('Location: ../index.php'); ?>
<?php else: ?>
    <?php header('Location: ../login.php'); ?>
<?php endif ?>