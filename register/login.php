<?php session_start(); 
    $_SESSION['registration'] = true;
?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin'): ?>
<?php include('../partials/header.php'); ?>
<?php include('../partials/message.php'); ?>
    <script src="../public/js/logout.js"></script>
    <form action="./php/start_session.php" method="post">
        <div class="field">
            <label class="label">ID Number</label>
            <input type="text" class="input" maxlength="6" name="idnumber">
        </div>
        <button type="button" id="logout" class="button">Log-Out</button>
        <button type="submit" class="button">Continue</button>
    </form>
<!-- Modal Start -->
<div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Log out</p>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
      <form action="./php/end_session.php" method="post">
            <div class="field">
                <label for="Username" class="label">Admin Username</label>
                <input type="text" class="input" name="username">
            </div>
            <div class="field">
                <label for="password" class="label">Password</label>
                <input type="password" class="input" name="password">
            </div>
            <button type="submit" class="button is-primary">Logout</button>
        </form>
    </section>
    <footer class="modal-card-foot">
      <button class="button button-close is-warning">Cancel</button>
    </footer>
  </div>
  <button class="modal-close is-large" aria-label="close"></button>
</div>
<?php include('../partials/footer.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./edit.php'); ?>
<?php else: ?>
    <?php header('Location: ../login.php'); ?>
<?php endif ?>