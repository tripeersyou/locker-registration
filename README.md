# Locker Registration System for DSWS

## Setting up the development environment.

* Download and Install XAMPP from the Apache Friends [website](https://apachefriends.org/download.html).
* Clone the git repository using HTTPS in the `xampp/htdocs` folder
* Open the XAMPP Control Panel and run Apache and MySQL.
* Open [http://localhost/phpMyAdmin](http://localhost/phpMyAdmin) in your web browser.
* In the SQL Tab of phpMyAdmin copy and paste the contents of `sql/structure.sql`.
* Access the website through [http://localhost/locker-registration](http://localhost/locker-registration).