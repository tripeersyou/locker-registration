<?php
    session_start();
    if($_POST){
        include_once('./db_connect.php');
        $statuses = $_POST['place_status'];
        $db->query("UPDATE places SET status = 0");
        for ($i=0; $i < count($statuses) ; $i++) { 
            $place_id = $statuses[$i];
            $db->query("UPDATE places SET status = 1 WHERE id = $place_id");
        }
        $_SESSION['message'] = 'Places successfully updated.';
        header('Location: ../settings.php');
    }
?>