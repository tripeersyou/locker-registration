<?php
    session_start();
    include_once('../../php/db_connect.php');
    if ($_POST) {
        $dsws_knowledge = htmlspecialchars($db->real_escape_string(implode(',', $_POST['dsws_knowledge'])));
        $registration_knowledge = htmlspecialchars($db->real_escape_string(implode(',', $_POST['registration_knowledge'])));
        $competence = htmlspecialchars($db->real_escape_string($_POST['competence']));
        $venue = htmlspecialchars($db->real_escape_string($_POST['venue']));
        $logistics = htmlspecialchars($db->real_escape_string($_POST['logistics']));
        $system_use = htmlspecialchars($db->real_escape_string($_POST['system_use']));
        $completion_time = htmlspecialchars($db->real_escape_string($_POST['completion_time']));
        $suggestion = htmlspecialchars($db->real_escape_string($_POST['suggestion']));
        $comments = htmlspecialchars($db->real_escape_string($_POST['comments']));
        $overall = htmlspecialchars($db->real_escape_string($_POST['overall']));
        $id = $_SESSION['id_number'];
        $query = "INSERT INTO evaluations (dsws_knowledge, registration_knowledge, competence, venue, logistics, system_use, completion_time, suggestion, comments, overall, student_id) VALUES('$dsws_knowledge', '$registration_knowledge', $competence, $venue, $logistics, $system_use, $completion_time, '$suggestion', '$comments', $overall, $id);";
        $db->query($query);
        $locker = $_SESSION['registered'];
        $query = "DELETE FROM queue WHERE student_id = $id";
        $db->query($query);
        $_SESSION['message'] = "Thank you very much for registering a locker and answering our evaluation, you have registered is locker: $locker.";
        unset($_SESSION['registered']);
        unset($_SESSION['edited']);
        unset($_SESSION['id_number']);
        $_SESSION['status'] = 'admin';
        header('Location: ../login.php');
    }