<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin' && !isset($_SESSION['registration'])): ?>
<?php include('./partials/header.php'); ?>
<?php include('./partials/message.php'); ?>
<?php include_once('./php/db_connect.php'); ?>
<script src="../public/js/registration.js"></script>
<?php 
    $query = "SELECT * from places WHERE status = 1";
    $result_building = $db->query($query);
    if (!isset($_GET['id'])){
        $_GET['id'] = -1;
    }
?>
<h1 class="title">Lockers</h1>
<a href="./index.php" class="button">Back</a>
<a href="./add_locker.php" class="button">Add Cabinet</a>
<div class="tabs is-medium">
  <ul>
    <?php while($place = $result_building->fetch_assoc()): ?>
        <?php if($_GET['id'] == $place['id']): ?>
            <li class="is-active"><a href="?id=<?= $place['id']?>"><?= $place['name']?></a></li>
        <?php else: ?>
            <li><a href="?id=<?= $place['id']?>"><?= $place['name']?></a></li>
        <?php endif; ?>
    <?php endwhile; ?>
  </ul>
</div>
<?php
    $place = htmlspecialchars($db->real_escape_string($_GET['id']));
    $query = "SELECT lockers.id, places.name, cabinets.room, cabinets.row_number, cabinets.position, cabinets.floor FROM lockers JOIN cabinets ON (cabinets.id = lockers.cabinet_id) JOIN places ON (places.id = cabinets.place_id) WHERE cabinets.place_id = $place";
    $result = $db->query($query);
    $lockers = $result->fetch_all(MYSQLI_NUM);
?>
    <?php if($lockers[0][1] == 'Berchman' || $lockers[0][1] == 'Kostka'): ?>
     <?php for ($i = 0; $i < count($lockers) ; $i++): ?>
        <?php if($i % 24 == 0): ?>
        <div style="text-align:center">
            <h2 class="title is-4" style="margin:0px;">In front of <?= $lockers[$i][2]?></h1>
            <small class="subtitle">Floor #<?= $lockers[$i][5]?> | <?= $lockers[$i][4] ?> side</small>
            <hr>
        </div>
        <?php endif ?>
        <?php if($i % 8 == 0): ?>
            <div class="columns is-multiline">
        <?php endif ?>
            <div class="column">
                <button class="button is-primary"><?= $lockers[$i][0]?></button>
            </div>
        <?php if($i % 8 == 7): ?>
            </div>
        <?php endif ?>
        <?php if($i % 24 == 0): ?>
            <br>
        <?php endif ?>
     <?php endfor ?>
    <?php elseif ($lockers[0][1] == 'PLDT-CTC'):?>
    <?php for ($i = 0; $i < count($lockers) ; $i++): ?>
        <?php if($i % 24 == 0): ?>
        <div style="text-align:center;">
            <h2 class="title is-4" style="margin:0px;">Near <?= $lockers[$i][2]?></h1>
            <small class="subtitle">Floor #<?= $lockers[$i][5]?></small>
            <hr>
        </div>
        <?php endif ?>
        <?php if($i % 8 == 0): ?>
            <div class="columns is-multiline">
        <?php endif ?>
            <div class="column">
                <button class="button is-primary"><?= $lockers[$i][0]?></button>
            </div>
        <?php if($i % 8 == 7): ?>
            </div>
        <?php endif ?>
        <?php if($i % 24 == 0): ?>
            <br>
        <?php endif ?>
     <?php endfor ?>
    <?php elseif ($lockers[0][1] == 'SEC-A'):?>
    <?php elseif ($lockers[0][1] == 'SEC-C'):?>
    <?php endif ?>
<?php include('./partials/footer.php'); ?>
<?php elseif(isset($_SESSION['registration'])): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php else: ?>
    <?php header('Location: ./login.php'); ?>
<?php endif ?>