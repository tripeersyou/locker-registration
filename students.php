<?php session_start(); ?>
<?php if(isset($_SESSION['status']) && $_SESSION['status'] == 'admin' && !isset($_SESSION['registration'])): ?>
<?php include('./partials/header.php'); ?>
<?php include('./partials/message.php'); ?>
<?php include_once('./php/db_connect.php'); 
    $query = "SELECT students.id, students.name, students.year, students.course, students.email, students.cellphone, students.locker_id, places.name AS place, students.registration_time FROM students JOIN lockers ON (lockers.id = students.locker_id) JOIN cabinets ON (cabinets.id = lockers.cabinet_id) JOIN places ON (places.id = cabinets.place_id);";
    $result = $db->query($query);
?>
<h1 class="is title">Students</h1>
<a href="./index.php" class="button">Back</a>
<a href="./queue.php" class="button">Queue Students</a>
<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Full Name</th>
            <th>Year</th>
            <th>Course</th>
            <th>Email</th>
            <th>Cellphone</th>
            <th>Locker</th>
            <th>Building</th>
            <th>Reg. Time</th>
        </tr>
    </thead>
    <tbody>
        <?php while($student = $result->fetch_assoc()): ?>
        <tr>
            <td><?= $student['id'] ?></td>
            <td><?= $student['name'] ?></td>
            <td><?= $student['year'] ?></td>
            <td><?= $student['course'] ?></td>
            <td><?= $student['email'] ?></td>
            <td><?= $student['cellphone'] ?></td>
            <td><?= $student['locker_id'] ?></td>
            <td><?= $student['place'] ?></td>
            <td><?= $student['registration_time']?></td>
        </tr>
    <?php endwhile?>
<?php include('./partials/footer.php'); ?>
<?php elseif(isset($_SESSION['registration'])): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php elseif(isset($_SESSION['status']) && $_SESSION['status'] == 'student'): ?>
    <?php header('Location: ./register/login.php'); ?>
<?php else: ?>
    <?php header('Location: ./login.php'); ?>
<?php endif ?>